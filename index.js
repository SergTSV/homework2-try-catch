//   синтаксична конструкція try...catch,  дозволяє нам “перехоплювати” помилки, що дає змогу скриптам виконати
//   потрібні дії, а не раптово припинити роботу
//   приклади - неправильно написаному програмному коді;
//    виконанні неприпустимих арифметичних операцій (наприклад, при діленні на нуль);
//    неналежному використанні порожнього посилання;
//    при посиланні запросу на сервер;
//    при вводі данних  та ін.





const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];


function createList(book) {
    const bookItem = document.createElement("li");
    const propertyList = document.createElement("ul");
    const root = document.querySelector('#root');
    const bookList = document.createElement("ul");
    root.append(bookList);

    for (const property in book) {
        const propertyItem = document.createElement("li");
        propertyItem.textContent = book[property];
        propertyList.append(propertyItem);
    }

    bookItem.append(propertyList);
    bookList.append(bookItem);
}

function checkСorrect(item) {
    item.forEach(book => {
        try {
            ["author", "name", "price"].forEach(property => {
                if (!book.hasOwnProperty(property)) {
                    throw new Error(`${property} is not found`);
                }
            });

            createList(book);
        } catch (e) {
            console.error(e);
        }
    });
}

checkСorrect(books);

